# Analyser_Merge specification

[Analyser_Merge](https://github.com/osm-fr/osmose-backend/blob/master/analysers/Analyser_Merge.py) is the base class for creating new open data analyzers in Osmose.

[Examples of analyzers](https://github.com/osm-fr/osmose-backend/tree/master/analysers) (files starting with `analyser_merge_*`).

## `Analyser_Merge`

Class allowing Osmose to conflate open datasets with existing OSM data. This is the global wrapper, which will contain all parameters.

```python
Analyser_Merge.__init__(
	self,				# Leave as is
	config,				# Leave as is
	logger,				# Leave as is
	url,				# Dataset documentation URL (unicode string)
	name,				# Official name of the dataset (without mention of producer or city) (unicode string)
	parser,				# Parser object, use one of the following class : CSV, JSON, GeoJSON, SHP, GTFS
	load,				# Load object
	mapping				# Mapping object
)
```

## `Parser`

Class allowing convert of various input formats into a standardized output. Each parser contains mainly two functions : one for listing columns (`header`) and one for inserting data into database (`import_`). Other helper functions can be present to simplify the parsing task.

### `CSV` parser

```python
CSV(
	source,					# Source object
	separator = u',',		# Separator character, by default `,`
	null = u'',				# Null value, by default empty string
	header = True,			# Is the first line the header, true by default
	quote = u'"',			# Quote character, `"` by default
	csv = True				# Should import on database use CSV syntax, true by default
)
```

### `JSON` and `GeoJSON` parsers

Both can be used similarly.

```python
JSON(
	source,							# Source object
	extractor = lambda json: json	# Extractor function, when data in JSON isn't at root (example `json: json['data']`)
)
```

### `SHP` and `GTFS` parsers

Both can be used similarly. They only require a `source` parameter.

```python
SHP(
	source		# Source object
)
```

## `Source`

Class for defining the source of the dataset. Every parameter is optional.

```python
Source(
	attribution = None,		# Dataset creator as string
	millesime = None,		# Date of the dataset as MM/YYYY date string
	encoding = "utf-8",		# File encoding
	file = None,			# File path on the local disk
	fileUrl = None,			# URL of the dataset (direct link)
	fileUrlCache = 30,		# Number of days to keep file in cache
	zip = None,				# Name of the interesting file in downloaded ZIP file (example `dataset.csv`)
	gzip = False,			# Set to `True` if downloaded file is Gzipped
	filter = None			# Python lambda expression for filtering some lines of the text file
)
```

## `Mapping`

Class for merging input dataset features with OSM features. It handles various objects for transforming and selecting data from both sides.

```python
Mapping(
	select = Select(),			# Select object (for choosing OSM data to compare)
	osmRef = "NULL",			# Name of OSM tag which is used as a reference for this dataset (example : `ref:UAI` for schools)
	conflationDistance = None,	# Distance for conflation (in meters) if no feature with osmRef has been found
	extraJoin = None,			# Extra tag to use for conflation (example : `addr:housenumber` for addresses)
	generate = Generate()		# Generate object (definition of transformation to apply on attributes)
)
```

## `Load`

Class for precising options for loading input dataset into database used by Osmose. In particular, options for choosing coordinates columns and filtering data.

```python
Load(
	x = ("NULL",),				# Column for longitude (as string if column name, or as `("SQL",)` if SQL query
	y = ("NULL",),				# Column for latitude (as string if column name, or as `("SQL",)` if SQL query
	srid = 4326,				# Projection EPSG code (optional)
	table_name = None,			# To change the default SQL table name (optional)
	create = None,				# SQL list of columns (optional)
	select = {},				# Python dictionnary to restrict rows to keep in Osmose (example `{"realisation": u"Réalisé","nature": [u"Arceau vélo", u"Rack", u"Potelet"]}`)
	uniq = None,				# List of columns for distinct select (as string array)
	where = lambda res: True,	# Python lambda function for filtering data (example: `lambda row: row["lat"] != "48.858858899999994" and row["lon"] != "2.3470599")`)
	map = lambda i: i,			# For replacing rows (optional)
	xFunction = lambda i: i,	# Python lambda function for preparing x column before projection. Can be `self.degree`, `self.float_comma` or a custom function
	yFunction = lambda i: i		# Python lambda function for preparing y column before projection. Can be `self.degree`, `self.float_comma` or a custom function
)
```

## `Select`

Class for choosing OSM data to compare your dataset with.

```python
Select(
	types = [ "nodes", "ways", "relations" ],	# Type of geometries
	tags = {}									# List of tags as python dictionnary, or an array of dictionnaries for combining possibilities (OR)
)
```

## `Generate`

Class for defining what the Osmose error shoud look like at the end.

```python
Generate(
	missing_official_fix = True,	# Allow quick fix
	static1 = {},					# Tags to embed in created object as-is (main tags)
	static2 = {},					# Tags to embed in created object as-is (secondary tags like name/ref/source... not checked at updates)
	mapping1 = {},					# Tags to embed and based on input dataset (main tags, as a python dictionnary)
	mapping2 = {},					# Tags to embed and based on input dataset (secondary tags, not checked at updates)
	tag_keep_multiple_values = [],	# Tags which existing values should be kept, and new values append (like cuisine, voltage...)
	text = lambda tags, fields: {}	# Label for the Osmose error (for user display)
)
```
